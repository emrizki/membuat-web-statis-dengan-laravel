<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function get_register() {
        return view('register');
    }
    public function post_register(Request $request) {
        // dd($request["first_name"]);
        $first_name = $request["first_name"];
        $last_name = $request["last_name"];

        return view('welcome', ["first_name" => $first_name, "last_name" => $last_name]);
    }
}
